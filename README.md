# Overview

A technical interview project for data engineers.

The objective is to write a python program that will collect as many logos as you can across across a sample of websites.


# Objectives

* Write a program that will crawl a list of website and output their logo URLs.
* The program should read domain names on `STDIN` and write a CSV of domain and logo URL to `STDOUT`.
* A `websites.csv` list is included as a sample to crawl.
* You can't always get it right, but try to keep precision and recall as high as you can. Be prepared to explain ways you can improve. Bonus points if you can measure.
* Be prepared to discuss the bottlenecks as you scale up to millions of websites. You don't need to implement all the optimizations, but be able to talk about the next steps to scale it for production.
* Spare your time on implementing features that would be time consuming, but make a note of them so we can discuss the ideas.
* Please implement using python.
* Please keep 3rd party dependencies to a minimum, unless you feel there's an essential reason to add a dependency.
* We use [Nix](https://nixos.org/nix/) for package management. If you add your dependencies to `default.nix`, then it's easy for us to run your code. Install nix and launch the environment with `nix-shell` (works Linux, MacOS, and most unixes). Or install dependencies however you're comfortable and give us instructions.

There's no time limit. Spend as much or as little time on it as you'd like. Fork this git repository, and share your fork when you're done. We'll schedule a follow-up call to review.


# Description

The project is implemented with `scrapy`, which offers all the necessary functionality
with all the necessary helpers for rapidly and optimally satisfying the project
necessities.

Run this command to request the logos:

    scrapy crawl logos < websites.csv >output.csv
    
    
The program runs asynchronously on every request and you can change the concurrency
on `logos/logos/settings.py`:

    CONCURRENT_REQUESTS = 20    # 20 concurrent requests

So it is ready for scaling up.

Measure and logging is also ready with default stats and logging. For showing
more logs we can change in settings:

    LOG_LEVEL = 'DEBUG'

## TODO

- Right now it is getting favicon information, which normally has a low quality. 
We can get better information with better heuristics on how we visit the site. 
- There is a [known error](https://github.com/scrapy/scrapy/issues/2905) 
if we increase a lot the `CONCURRENT_REQUESTS` on OSX, so thisis recommended right now
to be run inside a linux environment, but can be improved if it is necessary on OSX.
- Some domains are unreachable and we are just failing them, we could improve to also
return different or default results on those.
