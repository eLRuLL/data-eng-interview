"""Extra xpath functions.

Inspired by: https://gist.github.com/shirk3y/458224083ce5464627bc

"""
import logging
from lxml import etree

_XPATH_FUNCS = {}
_HASCLASS_EXPR = 'contains(concat(" ", @class, " "), " {} ")'

logger = logging.getLogger(__name__)


def register(func):
    """Method to register xpath functions."""
    fname = func.__name__.replace('_', '-')
    _XPATH_FUNCS[fname] = func
    return func


def setup():
    """Method to add registered functions for selector's usage."""
    fns = etree.FunctionNamespace(None)
    for name, func in _XPATH_FUNCS.items():
        fns[name] = func


@register
def has_class(context, *classes):
    """has-class function.

    Return True if all ``classes`` are present in element's class attr.

    """
    node_cls = context.context_node.get('class')
    if node_cls is None:
        return False
    node_cls = ' ' + node_cls + ' '
    for cls in classes:
        if ' ' + cls + ' ' not in node_cls:
            return False
    return True


@register
def lower_case(context, attributes):
    """has-class function.

    Returns an attribute transformed to lower-case.

    """
    return [attr.lower() for attr in attributes]


setup()
