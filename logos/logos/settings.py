# -*- coding: utf-8 -*-

# Scrapy settings for logos project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'logos'

SPIDER_MODULES = ['logos.logos.spiders']
NEWSPIDER_MODULE = 'logos.logos.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 ' \
             '(KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False
REFERER_ENABLED = False
DOWNLOAD_TIMEOUT = 15
LOG_LEVEL = 'DEBUG'

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 15

TELNETCONSOLE_ENABLED = False

FEED_URI = 'stdout:'
FEED_FORMAT = 'csv'
