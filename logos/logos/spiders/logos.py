import sys

from scrapy import Request, Spider

from urllib.parse import urlunparse, urlunsplit, urljoin, urlparse


class LogosSpider(Spider):
    """Spider to find website logos, send the domain lists as STDIN."""
    name = 'logos'

    def _get_domain_url(self, netloc):
        """Helper method to get a valid domain url."""
        return urlunsplit(['http', netloc, '', '', ''])

    def start_requests(self):
        """Starting point on the spider, iterate over domains and execute first request."""
        while True:
            domain = sys.stdin.readline()
            clean_domain_netloc = domain.strip()
            if not clean_domain_netloc:
                break

            domain_url = self._get_domain_url(clean_domain_netloc)

            yield Request(
                urljoin(domain_url, 'favicon.ico'),
                callback=self.parse_favicon,
                meta={
                    'domain': clean_domain_netloc,
                    'domain_url': domain_url,
                },
                errback=self.parse_error
            )

    def parse_error(self, failure):
        """Try requesting the domain homepage."""
        yield Request(
            failure.request.meta['domain_url'],
            callback=self.parse_homepage,
            meta={'domain': failure.request.meta['domain']},
            errback=self.parse_home_failure
        )

    def parse_home_failure(self, failure):
        """Try requesting the www sub-domain if it wasn't visited before."""
        parsed_url = urlparse(failure.request.url)
        if parsed_url.netloc.startswith('www.'):
            return

        replaced_url = parsed_url._replace(netloc='www.' + parsed_url.netloc)
        yield Request(
            urlunparse(replaced_url),
            callback=self.parse_homepage,
            meta={'domain': failure.request.meta['domain']},
        )

    def parse_favicon(self, response):
        """Parse favicon response."""
        yield {'domain': response.meta['domain'], 'logo_url': response.url}

    def parse_homepage(self, response):
        """Parse domain default icon section from homepage response."""
        icon_url = response.xpath(
            '//link[contains(lower-case(@rel), "icon")]/@href'
        ).extract_first()
        yield {'domain': response.meta['domain'], 'logo_url': response.urljoin(icon_url)}
